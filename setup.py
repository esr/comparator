#!/usr/bin/env python3

from distutils.core import setup
setup(name="comparator",
      version="2.0",
      py_modules=["comparator"])
